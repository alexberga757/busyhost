# Busyhost

<h3> Giới thiệu </h3>
đây là một dự án do tôi,hay Lê Ngọc Cương thường gọi là Belling phát triển,nó cơ bản là thay thế cho hệ thống init của linux và được viết bằng shell script (do tôi đéo biết tạo một môi trường chroot nên nó như này :D) 

<h3> Cơ Bản </h3>
- Hãy tạo một tệp service trong /etc/uhost/service ví dụ như busybost-shell.host:

```sh
#!/bin/busybox sh
case $1 in
	start)
		run="/bin/busybox sh"
		$run
		;;
	remove)
		echo ""
	*)
		echo ""
		;;
esac
```

- Nếu bạn muốn kích hoạt dịch vụ thì: `busybox sh /etc/uhost/busyhost busybost-shell.host enable`
- Còn ngược lại là: `busybox sh /etc/uhost/busyhost busybost-shell.host disable`

* Lưu ý nhỏ:
`busybox sh /etc/uhost/busyhost busybost-shell.host start`  hoặc `busybox sh /etc/uhost/busyhost busybost-shell.host stop` thì busybox không bắt buộc bạn chọn `start` hoặc `stop` ,bạn có thể tùy chỉnh bất kỳ điều gì ngoài `enable` hoặc `disable`,ví dụ như `run` hoặc `exit`

<h3> Busyhost Tools </h3>

- Busyhost tools là lệnh chạy các tools cơ bản của busyhost: `/etc/uhost/busyhost-tool` 

1. `list all/enable`: là lệnh liệt kê các dịch vụ có sẵn trong hệ thống bao gồm các dịch vụ đã được kích hoạt và tất cả các dịch vụ.Trong đó `list all` là liệt kê tất cả các dịch vụ có sẵn bao gồm các dịch vụ chưa kích hoạt và đã kích hoạt,`list enable` là liệt kê tất cả cá dịch vụ đã được kích hoạt bởi người dùng hoặc hệ thống trong `/etc/uhost/enable`
2. `add-auto <file-name> <service-name>`: thêm dịch vụ tự động khởi động tệp (môi trường thực thi: `/bin/busybox sh`)
3. `remove-service <service-name>`: xóa một dịch vụ nào đó,trước khi xóa thì kiểm tra nó đã được `disable` hay chưa.Trước khi xóa dịch vụ/service thì nó sẽ gửi đến service với dòng lệnh `remove` vì thế  nên bạn không được dùng `busyhost <service-name> remove` nó có thể ảnh hưởng đến dịch vụ của bạn. **⚠️ CẢNH BÁO: Trước khi xóa hãy đảm bảo rằng nó KHÔNG QUAN TRỌNG để đảm bảo không gặp vấn đề gì trong hệ thống**. 

<h3> Yêu cầu </h3>

- chỉ có một yêu cầu duy nhất đó là bạn hãy sử dụng gói `busybox` hoặc bạn có thể tùy chỉnh mã nguồn thành `/bin/sh` hoặc `/bin/bash` nếu muốn.Có thể sẽ có các gói mã nguồn về sau được thêm vào,để biết thêm thông tin về Busyhost nói riêng và các dự án khác của tôi nói chung thì hãy liên hệ tôi qua: [carrd](https://lengoccuong.carrd.co)